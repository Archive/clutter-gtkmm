/* Copyright (C) 2014 The cluttermm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtkmm/window.h>
#include <cluttermm/actor.h>

_DEFS(clutter-gtkmm,clutter-gtk)
_PINCLUDE(gtkmm/private/window_p.h)


namespace Clutter
{
namespace Gtk
{

/** A Gtk::Window that embeds its contents onto a stage.
 *
 * Clutter::Gtk::Window is a Gtk::Window sub-class that embeds a Clutter stage.
 *
 * Clutter::Gtk::Window behaves exactly like a Gtk::Window, except that its
 * child is automatically embedded inside a Clutter::Gtk::Actor and it is
 * thus part of the embedded Clutter::Stage.
 *
 * Clutter actors can be added to the same stage by calling
 * get_stage().
 */
class Window : public ::Gtk::Window
{
  _CLASS_GTKOBJECT(Window, GtkClutterWindow, GTK_CLUTTER_WINDOW, ::Gtk::Window, GtkWindow)

public:
  _CTOR_DEFAULT

#m4 _CONVERSION(`ClutterActor*',`Glib::RefPtr<Clutter::Actor>',`Glib::wrap($3)')
  _WRAP_METHOD(Glib::RefPtr<Clutter::Actor> get_stage(), gtk_clutter_window_get_stage, refreturn)
  _WRAP_METHOD(Glib::RefPtr<const Clutter::Actor> get_stage() const, gtk_clutter_window_get_stage, refreturn, constversion)
};

} // namespace Gtk
} // namespace Clutter
